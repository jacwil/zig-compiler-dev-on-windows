<#
  .SYNOPSIS
  Script to upgrade to latest Zig compiler from master branch.

  .DESCRIPTION
  Script to upgrade to latest Zig compiler from master branch.
#>

$ErrorActionPreference = "Stop"

Add-Type -AssemblyName System.Web

function Join-Paths([string]$initPath,[string[]]$childNames) {
  $p = $initPath
  foreach ($childName in $childNames) {
    $p = Join-Path $p $childName
  }
  $p
}

# Get index.json listing zig download urls
$destJson = Join-Path $PSScriptRoot "zig-latest.json"
Invoke-WebRequest -Uri "https://ziglang.org/download/index.json" -OutFile $destJson
$json = Get-Content $destJson | ConvertFrom-Json
Remove-Item -Force $destJson

# This gets either the current zig version or the error output for zig not being found.
# We then compare the version to the latest in index.json and if they mismatch, upgrade.
$currentZigVersion = @(cmd /c zig version '2>&1')[0]
$newZigVersion = $json.master.version
$publishDate = $json.master.date
if ($currentZigVersion -eq $newZigVersion) {
  Write-Host "Zig is already at its latest version ${currentZigVersion}, published on ${publishDate}."
  exit
}

# The output of 'zig version' is either a version number or an error message. Skip the error message.
if (-not $currentZigVersion.Contains("is not recognized")) {
  Write-Host "Zig current version is ${currentZigVersion}. Updating zig-latest to ${newZigVersion}, published on ${publishDate}."
}
else {
  Write-Host "Fetching zig-latest ${newZigVersion}, published on ${publishDate}."
}

$extractionPath = Join-Path $PSScriptRoot "zig-windows-x86_64-$newZigVersion"
if (-not (Test-Path $extractionPath)) {
  # Download latest Windows zig binaries
  $destZip = "${extractionPath}.zip"
  if (-not (Test-Path $destZip)) {
    $latestUrl = $json.master.'x86_64-windows'.tarball
    Write-Host "Invoke-WebRequest" -Uri $latestUrl -OutFile $destZip
    Invoke-WebRequest -Uri $latestUrl -OutFile $destZip
  }

  # Extract files. Zip file when extracted will be a single directory with name "zig-windows-x86_64-${newZigVersion}"
  try {
    $sevenZipExe = Join-Paths $Env:Programfiles @("7-zip", "7z.exe")
    if ((Test-Path $sevenZipExe)) {
      Write-Host "Extracting zip contents using 7zip..."
      & $sevenZipExe e $destZip -spf -y -o"$PSScriptRoot"
    } else {
      Write-Host "Extracting zip contents..."
      Expand-Archive $destZip -DestinationPath $PSScriptRoot
    }
  }
  finally {
    Remove-Item -Force -Path $destZip | Out-Null
  }
}

try {
  # Delete old directory
  $destPath = Join-Path $PSScriptRoot "zig-latest"
  if ((Test-Path $destPath)) {
    Write-Host "Deleting stale cache of zig-latest."
    Remove-Item -Recurse -Force $destPath | Out-Null
  }

  Move-Item -Path $extractionPath -Destination $destPath | Out-Null
}
finally {
  if ((Test-Path $extractionPath)) {
    Remove-Item -Recurse -Force $extractionPath | Out-Null
  }
}

Write-Host "Download and extraction of zig-latest complete. Version ${newZigVersion} published ${publishDate}."
