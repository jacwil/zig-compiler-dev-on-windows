<#
  .SYNOPSIS
  Script to fetch latest ZLS source and build it.

  .DESCRIPTION
  Script to upgrade a clone of the ZLS source and build the latest version of it.
#>

$ErrorActionPreference = "Stop"

function Join-Paths([string]$initPath,[string[]]$childNames) {
  $p = $initPath
  foreach ($childName in $childNames) {
    $p = Join-Path $p $childName
  }
  $p
}

# Needed for restoring the current working directory later
$currentWorkingDirectory = Get-Location

$zigLatest = Join-Paths $PSScriptRoot @("zig-latest", "zig.exe")
if (-not (Test-Path $zigLatest)) {
  Write-Error "Unable to find cached zig-latest. You should run `zigup` to download it."
}

$clonePath = Join-Path $PSScriptRoot "zls"
if (-not (Test-Path $clonePath)) {
  & git clone https://github.com/zigtools/zls "$clonePath"
}

Set-Location $clonePath
try {
  Write-Host "Pulling latest commit from github.com/zigtools/zls"
  & git pull
  Write-Host "Building zls.exe"
  & "$zigLatest" build -Doptimize=ReleaseSafe
}
finally {
  Set-Location $currentWorkingDirectory
}

$zlsExecutable = Join-Paths $clonePath @("zig-out", "bin", "zls.exe")
if (-not (Test-Path $zlsExecutable)) {
  Write-Error "Failed to build zls.exe"
}

Write-Host "Update of zls complete. Available at $zlsExecutable"
