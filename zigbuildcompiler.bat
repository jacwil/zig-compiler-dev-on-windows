@PUSHD "%ZIGSRC%"
"%ZIGTOOLS%zig-latest\zig.exe" build -p stage3 --search-prefix "%ZIGTOOLS%zig-compilerdevkit" --zig-lib-dir "%ZIGTOOLS%zig-latest\lib" -Dstatic-llvm -Duse-zig-libcxx -Dtarget=x86_64-windows-gnu -Doptimize=ReleaseSafe
@SET ERRCODE=%ERRORLEVEL%
@POPD
@IF NOT %ERRCODE% == 0 ( EXIT /B %ERRCODE% )

@PUSHD "%ZIGSRC%"
@RMDIR /S /Q "%ZIGTOOLS%zig-dev"
@MKDIR "%ZIGTOOLS%zig-dev"
xcopy.exe /E /K /H /I stage3\lib "%ZIGTOOLS%zig-dev\lib"
COPY stage3\bin\zig.exe "%ZIGTOOLS%zig-dev"
@POPD
