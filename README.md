Some random build scripts to make building the compiler and sync'ing with latest master branch of
https://github.com/ziglang/zig less work.

Do feel free to open issues if you see a better way. These scripts avoid needing to install llvm, cmake, ninja, etc
installed on Windows and should be mostly self-contained. These scripts follow **Option 1b: Zig build** described in
[Building Zig on Windows](https://github.com/ziglang/zig/wiki/Building-Zig-on-Windows).

## Using the scripts

There are two versions of the Zig compiler you can switch between using `uselatest` and `usedev`: **zig-latest** for the
raw untainted Zig compiler from Ziglang.org and **zig-dev** for your local build. This is compiled from source locally
with any changes you may have made.

First, edit `init.cmd` to point to the correct `ZIGSRC` directory where you have your clone of https://github.com/ziglang/zig.

Next, `ZIGTOOLS` should point to where you want to cache the latest Zig artifact from Ziglang.org and the locally
compiled versions, but at the moment the PowerShell scripts do not respect the value. Should be an easy-ish fix if the
script directory is an issue.

Update your User (or System) environment variables to include `C:\src\zig-compiler-dev-on-windows\zig-latest` in the
PATH environment variable where `C:\src\zig-compiler-dev-on-windows\` is the path you cloned these scripts. See
[about_Environment_Variables](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_environment_variables).

Open `init.cmd` by double-clicking on it. I like to make a shortcut on my desktop: `C:\src\zig-compiler-dev-on-windows\init.cmd`.

If you are only wanting the latest Zig compiler, `zigup` is all you really need to stay sync'd. But for compiler
development you will want to run `zigupall` and then `zigbuildcompiler` to get everything setup the first time.

The `init.cmd` initializes aliases in the command prompt. The complete list can be viewed by running `doskey /macros`.
- `uselatest`: Update PATH to prefer the **zig-latest** compiler. Run `where zig` to verify.
- `usedev`: Update PATH to prefer the **zig-dev** compiler. Run `where zig` to verify.
- `zigup`: Sync's **zig-latest** to the latest version distributed on Ziglang.org. `zigupall` is preferred if doing
    compiler development, but this is useful if you just want to use Zig's latest master branch.
- `zigupall`: Update both **zig-latest** and the [Windows Zig Compiler Dev Kit](https://github.com/ziglang/zig/wiki/Building-Zig-on-Windows)
    to the latest versions.
- `zigbuildcompiler`: Compiles Zig from `ZIGSRC` and copies the output to **zig-dev**.
- `zlsup`: Sync's with master branch of [Zig Language Server](https://github.com/zigtools/zls) and builds it. This does
    not configure VSCode or other editors.

### Zig Language Server (ZLS)
If you intend to use the latest ZLS build with `zlsup`, you may need to update your editor's path to both zls.exe and
zig.exe.

For example, VSCode needs entries similar to the following in `%APPDATA%\Code\User\settings.json`. This is normally and
preferably opened using the Command Palette (Ctrl+Shift+P) then selecting "Preferences: Open User Settings (JSON)".
`"C:\src\zig-compiler-dev-on-windows` is the clone path of this repo, which should be replaced with your clone path.
This assumes you have the official Zig Language extension of VSCode installed as recommended by the
[ZLS Installation Guide](https://zigtools.org/zls/install).
```
"zig.path": "C:\\src\\zig-compiler-dev-on-windows\\zig-latest\\zig.exe",
"zig.zls.path": "C:\\src\\zig-compiler-dev-on-windows\\zls\\zig-out\\bin\\zls.exe",
"zig.zls.zigLibPath": "C:\\src\\zig-compiler-dev-on-windows\\zig-latest\\lib"
```

If you need to restore the above values to the VSCode extension defaults, you can open Extension settings of Zig
Language and hover over a setting to reset to make the Gear icon appear, clicking the Gear icon to open a context menu
with option to Reset Setting. You can also directly delete the entries in settings.json.

## Features

Checks to see if **zig-latest** is already at the latest version before downloading it again. Same check for the
Windows Zig Compiler Dev Kit.

## Known Issues

Zig will cache dependencies and build output to speed up compile times. It might be necessary to clear this cache in
local appdata and the repo's cache by using the `zigcleancompiler` macro.

If you pull the latest master branch in `ZIGSRC`, you might need to run `zigupall` and `zigcleancompiler` to resolve
compilation errors.

This script has assumptions about how to get the latest Windows Zig Compiler Dev Kit. Expect instability.
