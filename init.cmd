@echo off

REM Edit these before using these scripts
SET ZIGSRC=C:\src\zig

SET SCRIPTPATH=%~dp0
SET USRPATH=%PATH%

REM Because I have the PowerShell scripts hardcoded at the moment. This can be
REM fixed to respect the environment variable.
SET ZIGTOOLS=%SCRIPTPATH%

REM Setup command aliases
doskey /macrofile="%SCRIPTPATH%macros.txt"

cmd /k %*
