<#
  .SYNOPSIS
  Script to upgrade dev tools needed to compile Zig on Windows.

  .DESCRIPTION
  Script to upgrade dev tools needed to compile Zig on Windows.
#>

$ErrorActionPreference = "Stop"

Add-Type -AssemblyName System.Web

function RefreshCompilerDevKit {
  param (
    $ZigCompilerDevKitPath,
    $ZigCompilerDevKitUrl,
    $ZigCompilerDevKitVersion
  )

  $versionFile = Join-Path $ZigCompilerDevKitPath "devkitversion.txt"
  if ((Test-Path $versionFile)) {
    $currentVersion = Get-Content $versionFile
    if ($currentVersion -eq $ZigCompilerDevKitVersion) {
      Write-Host "Zig compiler dev kit already at latest version ${ZigCompilerDevKitVersion}."
      return
    }

    Write-Host "Zig compiler dev kit is at ${currentVersion}. Upgrading to ${ZigCompilerDevKitVersion}."
  }

  $extractionPath = Join-Path $PSScriptRoot $ZigCompilerDevKitVersion
  if (-not (Test-Path $extractionPath)) {
    # Download the compiler dev kit
    $destZip = Join-Path $PSScriptRoot "${ZigCompilerDevKitVersion}.zip"
    if (-not (Test-Path $destZip)) {
      Write-Host "Invoke-WebRequest" -Uri $ZigCompilerDevKitUrl -OutFile $destZip
      Invoke-WebRequest -Uri $ZigCompilerDevKitUrl -OutFile $destZip
    }
  
    # Extract files. Zip file when extracted will be a single directory with name "$ZigCompilerDevKitVersion"
    # Example: zig+llvm+lld+clang-x86_64-windows-gnu-0.12.0-dev.2073+402fe565a
    try {
      $sevenZipExe = Join-Path (Join-Path $Env:Programfiles "7-zip") "7z.exe"
      if ((Test-Path $sevenZipExe)) {
        Write-Host "Extracting zip contents using 7zip..."
        & $sevenZipExe e $destZip -spf -y -o"$PSScriptRoot"
      } else {
        Write-Host "Extracting zip contents..."
        Expand-Archive $destZip -DestinationPath $PSScriptRoot
      }
    }
    finally {
      Remove-Item -Force -Path $destZip | Out-Null
    }
  }

  try {
    # Delete old directory
    if ((Test-Path $ZigCompilerDevKitPath)) {
      Write-Host "Deleting existing zig-devkit install."
      Remove-Item -Recurse -Force $ZigCompilerDevKitPath | Out-Null
    }
  
    Move-Item -Path $extractionPath -Destination $ZigCompilerDevKitPath | Out-Null

    # Write version file
    Set-Content -Path $versionFile -Value $ZigCompilerDevKitVersion | Out-Null
  }
  finally {
    if ((Test-Path $extractionPath)) {
      Remove-Item -Recurse -Force $extractionPath | Out-Null
    }
  }
}

function RefreshCompilerLib {
  param (
    $ZigCompilerDevKitPath,
    $ZigLatestPath
  )

  $compilerVersionFile = Join-Path $ZigCompilerDevKitPath "compilerversion.txt"
  if ((Test-Path $compilerVersionFile)) {
    $currentZigVersion = @(cmd /c "$ZigLatestPath\zig.exe" version '2>&1')[0]
    $currentCompilerLibVersion = Get-Content $compilerVersionFile
    if ($currentCompilerLibVersion -eq $currentZigVersion) {
      Write-Host "Zig compiler lib already at latest version ${currentZigVersion}."
      return
    }
  
    Remove-Item -Force $compilerVersionFile | Out-Null
    Write-Host "Zig compiler lib is at ${currentCompilerLibVersion}. Upgrading to ${currentZigVersion}."
  }

  # Overwrite lib directory
  Write-Host "Copying zig-latest/lib directory to Zig compiler dev kit 'lib' directory..."
  Copy-Item -Recurse -Force -Path (Join-Path $ZigLatestPath "lib") -Destination $ZigCompilerDevKitPath

  # Write version file
  Set-Content -Path $compilerVersionFile -Value $currentZigVersion | Out-Null
}


# Prerequisite
$zigLatestPath = Join-Path $PSScriptRoot "zig-latest"
if (-not (Test-Path $zigLatestPath)) {
  & $PSScriptRoot\zig-upgrade.ps1
}

# Download CI script locally to parse
$ciScriptPath = Join-Path $PSScriptRoot "ciscript.txt"
Invoke-WebRequest -Uri https://raw.githubusercontent.com/ziglang/zig/master/ci/x86_64-windows-debug.ps1 -OutFile $ciScriptPath

$zigCompilerDevKitVersion = ""
$zigCompilerDevKitUrl = ""
try {
  foreach ($line in (Get-Content $ciScriptPath)) {
    if ($line.StartsWith('$ZIG_LLVM_CLANG_LLD_NAME')) {
      $zigCompilerDevKitVersion = $line.Split('=')[1].Trim().Trim('"').Replace('$TARGET', 'x86_64-windows-gnu')
      break
    }
  }

  foreach ($line in (Get-Content $ciScriptPath)) {
    if ($line.StartsWith('$ZIG_LLVM_CLANG_LLD_URL')) {
      $zigCompilerDevKitUrl = $line.Split('=')[1].Trim().Trim('"').Replace('$ZIG_LLVM_CLANG_LLD_NAME', [System.Web.HttpUtility]::UrlEncode($zigCompilerDevKitVersion))
      break
    }
  }
}
finally {
  Remove-Item -Force $ciScriptPath | Out-Null
}

if (-not $zigCompilerDevKitVersion -or -not $zigCompilerDevKitUrl) {
  Write-Error "Unable to extract Zig compiler dev kit from CI script. Update needed for zig-upgrade-devchain.ps1."
}

$destPath = Join-Path $PSScriptRoot "zig-compilerdevkit"
RefreshCompilerDevKit -ZigCompilerDevKitPath $destPath -ZigCompilerDevKitUrl $zigCompilerDevKitUrl -ZigCompilerDevKitVersion $zigCompilerDevKitVersion
RefreshCompilerLib -ZigCompilerDevKitPath $destPath -ZigLatestPath $zigLatestPath

Write-Host "Download and extraction of Zig compiler dev kit complete."
